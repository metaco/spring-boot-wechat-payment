package club.example.wechatpayment.service;

import club.example.wechatpayment.core.param.OrderQueryParam;
import club.example.wechatpayment.core.param.UnifiedOrderParam;
import club.example.wechatpayment.core.result.NotificationResult;
import club.example.wechatpayment.core.result.OrderQueryResult;
import club.example.wechatpayment.core.result.UnifiedOrderResult;
import club.example.wechatpayment.service.xmlversion.impl.WeChatPayCoreServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WeChatPayCoreServiceTest  {

    @Autowired
    private WeChatPayCoreServiceImpl weChatPayCoreService;

    @Test
    public void testUnified() {
        UnifiedOrderParam orderParam = new UnifiedOrderParam();
        orderParam.setDeviceInfo("app-v");
        orderParam.setSign("");
        orderParam.setSignType("MD5");
        orderParam.setBody("测试支付");
        orderParam.setOutTradeNo("923845928592489634906806");
        orderParam.setFeeType("CNY");
        orderParam.setTotalFee(10);
        orderParam.setSpBillCreateIp("127.0.0.1");
        orderParam.setTradeType("APP");

        UnifiedOrderResult unifiedOrderResult = weChatPayCoreService.unifiedOrder(orderParam);
        Assert.assertNotNull(unifiedOrderResult);
    }

    @Test
    public void testOrder() {
        OrderQueryParam orderQueryParam = new OrderQueryParam();
        orderQueryParam.setOutTradeNo("923845928592489634906806");
        OrderQueryResult orderQueryResult = weChatPayCoreService.queryOrder(orderQueryParam);
        Assert.assertNotNull(orderQueryResult);
    }

    @Test
    public void testNotify() {
        String xml = "<xml><appid><![CDATA[wx9abed099f0585556]]></appid><attach><![CDATA[{\"orderType\":1,\"companyId\":3,\"orderId\":22561,\"payId\":2,\"couponId\":0,\"activityType\":0}]]></attach><bank_type><![CDATA[OTHERS]]></bank_type><cash_fee><![CDATA[162353]]></cash_fee><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[N]]></is_subscribe><mch_id><![CDATA[1509559041]]></mch_id><nonce_str><![CDATA[49xiarspez4hqzgpdu87c8yjfzu78f6w]]></nonce_str><openid><![CDATA[ogplK1OXhWOhm-Dx5aJel3RBA8AY]]></openid><out_trade_no><![CDATA[b19050516152916637484671hsmb]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[CCA2BA732D498366E86D8833B1C2DCA0]]></sign><time_end><![CDATA[20200401164031]]></time_end><total_fee>162353</total_fee><trade_type><![CDATA[APP]]></trade_type><transaction_id><![CDATA[4200000483202004011953533263]]></transaction_id></xml>";

        NotificationResult notificationResult = weChatPayCoreService.processNotification(xml);
        Assert.assertNotNull(notificationResult);
    }
}
