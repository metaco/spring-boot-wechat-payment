package club.example.wechatpayment.service;

import club.example.starter.service.StarterSplitService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SplitServiceTest {

    @Autowired
    private StarterSplitService starterSplitService;

    @Test
    public void testSplit() {
        List<String> strings =
                starterSplitService.split("cb,b,nb");
        Assert.assertNotEquals(0, strings.size());
    }
}
