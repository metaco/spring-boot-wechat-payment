package club.example.wechatpayment.endpoint;

import club.example.wechatpayment.task.AsyncMockDataTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@RestController
@RequestMapping("/rest/async/task")
public class SpringRestAsyncServiceEndpoint {

    private final AsyncMockDataTask asyncMockDataTask;

    public SpringRestAsyncServiceEndpoint(AsyncMockDataTask asyncMockDataTask) {
        this.asyncMockDataTask = asyncMockDataTask;
    }

    @GetMapping
    public String processing() {
        asyncMockDataTask.processingMockData();
        Future<Integer> integerFuture = asyncMockDataTask.processingMockDataInteger();
        try {
            return integerFuture.get().toString();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}
