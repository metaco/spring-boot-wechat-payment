package club.example.wechatpayment.provider;

import club.example.wechatpayment.core.param.OrderQueryParam;
import club.example.wechatpayment.core.param.UnifiedOrderParam;
import club.example.wechatpayment.properties.WeChatPaymentProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.security.SecureRandom;

public class ServiceParametersProvider {

    private final static XmlMapper xmlMapper = new XmlMapper();

    private final static SecureRandom secureRandom = new SecureRandom();

    private final WeChatPaymentProperties paymentProperties;

    private final PaymentServiceSignProvider signProvider;

    public ServiceParametersProvider(WeChatPaymentProperties paymentProperties,
                                     PaymentServiceSignProvider signProvider) {
        this.paymentProperties = paymentProperties;
        this.signProvider = signProvider;
    }

    static {
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public String provide(UnifiedOrderParam param) throws JsonProcessingException {
        param.setAppId(paymentProperties.getAppId());
        param.setMchId(paymentProperties.getMchId());
        // 构建 随机数
        long nonceLong = secureRandom.nextLong();

        param.setNonceStr(String.valueOf(nonceLong));
        param.setNotifyUrl(paymentProperties.getNotifyUrl());
        String sign = signProvider.provide(param, UnifiedOrderParam.class);
        param.setSign(sign);
        return writeAsXml(param);
    }

    public String provide(OrderQueryParam param) throws JsonProcessingException {
        param.setMchId(paymentProperties.getMchId());
        param.setAppId(paymentProperties.getAppId());
        // 构建 随机数
        long nonceLong = secureRandom.nextLong();

        param.setNonceStr(String.valueOf(nonceLong));
        String sign = signProvider.provide(param, OrderQueryParam.class);
        param.setSign(sign);
        return writeAsXml(param);
    }

    private String writeAsXml(Object param) throws JsonProcessingException {
        if (null == param) {
            throw new IllegalArgumentException("参数异常");
        }
        return xmlMapper.writeValueAsString(param);
    }
}