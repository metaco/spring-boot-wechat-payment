package club.example.wechatpayment.provider;

import club.example.wechatpayment.properties.WeChatPaymentProperties;
import club.example.wechatpayment.core.annotation.SignIgnoreProperty;
import club.example.wechatpayment.core.annotation.SignProperty;
import club.example.wechatpayment.core.constant.WeChatPayParameters;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Slf4j
public class PaymentServiceSignProvider {

    private final WeChatPaymentProperties paymentProperties;

    public PaymentServiceSignProvider(WeChatPaymentProperties paymentProperties) {
        Assert.notNull(paymentProperties, "paymentProperties could not be null");
        this.paymentProperties = paymentProperties;
    }

    public String provide(Object obj, Class<?> classType) {
        Map<String, String> generateSignParamMap = new TreeMap<>();
        Field[] declaredFields = classType.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            SignIgnoreProperty ignoreProperty = declaredField.getAnnotation(SignIgnoreProperty.class);
            if (null == ignoreProperty) {
                // 获取 RequestProperty 注解
                SignProperty annotation = declaredField.getAnnotation(SignProperty.class);
                // 判定 required 属性如果 为false 则不能获取该值
                Object fieldValue;
                try {
                    fieldValue = declaredField.get(obj);
                    String fieldName = declaredField.getName();
                    validateField(generateSignParamMap, annotation, fieldValue, fieldName);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        String sign = buildSign(generateSignParamMap);
        log.info("generate sign = {}", sign);
        return sign;
    }

    private void validateField(Map<String, String> generateSignParamMap,
                               SignProperty annotation, Object fieldValue, String fieldName) {
        if (null != annotation && annotation.required() && null == fieldValue) {
            throw new IllegalArgumentException("参数[" + fieldName + "]不能为空");
        }
        if (null == annotation && null != fieldValue) {
            generateSignParamMap.put(fieldName, String.valueOf(fieldValue));
        }
        if (null != annotation && null != fieldValue) {
            generateSignParamMap.put(annotation.value(), String.valueOf(fieldValue));
        }
    }

    private String buildSign(Map<String, String> generateSignParamMap) {
        String joinParamStr = generateSignParamMap.entrySet()
                .stream()
                .map(e -> e.getKey() + WeChatPayParameters.EQUALS_CHAR + e.getValue())
                .collect(Collectors.joining(WeChatPayParameters.AND_CHAR));

        StringBuilder stringBuilder = new StringBuilder(joinParamStr);
        stringBuilder.append(WeChatPayParameters.AND_CHAR)
                .append(WeChatPayParameters.SIGN_KEY)
                .append(WeChatPayParameters.EQUALS_CHAR)
                .append(paymentProperties.getMchKey());
        log.debug("join str = {}", stringBuilder.toString());

        byte[] bytes = stringBuilder.toString().getBytes(StandardCharsets.UTF_8);
        return DigestUtils.md5DigestAsHex(bytes).toUpperCase();
    }
}
