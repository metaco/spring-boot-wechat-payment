package club.example.wechatpayment.configure;

import club.example.wechatpayment.handler.HttpRequestHandler;
import club.example.wechatpayment.handler.HttpRequestXMLHandler;
import club.example.wechatpayment.properties.WeChatPaymentProperties;
import club.example.wechatpayment.provider.PaymentServiceSignProvider;
import club.example.wechatpayment.provider.ServiceParametersProvider;
import club.example.wechatpayment.service.xmlversion.WeChatPayCoreService;
import club.example.wechatpayment.service.xmlversion.impl.WeChatPayCoreServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"club.example.wechatpayment"})
@EnableConfigurationProperties(WeChatPaymentProperties.class)
@ConditionalOnClass(value = {WeChatPayCoreService.class, WeChatPayCoreServiceImpl.class})
public class WeChatPaymentAutoConfiguration {

    private final WeChatPaymentProperties weChatPaymentProperties;

    private final HttpRequestHandler httpRequestHandler;

    public WeChatPaymentAutoConfiguration(
            WeChatPaymentProperties weChatPaymentProperties,
            @Qualifier("httpRequestXMLHandler") HttpRequestHandler httpRequestHandler) {
        this.weChatPaymentProperties = weChatPaymentProperties;
        this.httpRequestHandler = httpRequestHandler;
    }

    @Bean
    @ConditionalOnMissingBean
    public WeChatPayCoreService weChatPayCoreService() {
        return new WeChatPayCoreServiceImpl(
                serviceParametersProvider(),
                paymentServiceSignProvider(), httpRequestHandler);
    }

    private PaymentServiceSignProvider paymentServiceSignProvider() {
        return new PaymentServiceSignProvider(weChatPaymentProperties);
    }

    private ServiceParametersProvider serviceParametersProvider() {
        return new ServiceParametersProvider(weChatPaymentProperties,
                paymentServiceSignProvider());
    }
}
