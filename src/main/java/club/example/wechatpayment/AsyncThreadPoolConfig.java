package club.example.wechatpayment;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * TODO: // 默认情况下 spring boot 异步线程池配置，线程不能重新利用
 *     // 非常浪费资源 ，所以编写异步服务，必须自己定义异步线程池
 */
@Slf4j
@Configuration
public class AsyncThreadPoolConfig implements AsyncConfigurer {
    /**
     * 定义异步线程池
     * @return Executor
     */
    @Bean
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor threadPoolExecutor = new ThreadPoolTaskExecutor();
        threadPoolExecutor.setCorePoolSize(5);
        threadPoolExecutor.setMaxPoolSize(10);
        threadPoolExecutor.setQueueCapacity(20);
        threadPoolExecutor.setKeepAliveSeconds(60);
        threadPoolExecutor.setThreadNamePrefix("WeChatPayMockTask-");

        // 是否所有等待所有线程执行完毕 关闭线程池 ()
        threadPoolExecutor.setWaitForTasksToCompleteOnShutdown(true);
        // 等待所有线程执行最终的时长
        threadPoolExecutor.setAwaitTerminationSeconds(60);
        // 拒绝执行策略
        threadPoolExecutor.setRejectedExecutionHandler(
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
        threadPoolExecutor.initialize();
        return threadPoolExecutor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new AsyncExceptionHandler();
    }

    static class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
        @Override
        public void handleUncaughtException(Throwable ex, Method method, Object... params) {
            log.info("Async error {}, method {}, params : {}", ex.getMessage(), method.getName(), params.toString());
            ex.printStackTrace();
        }
    }
}
