package club.example.wechatpayment.service.xmlversion.impl;

import club.example.wechatpayment.core.param.OrderQueryParam;
import club.example.wechatpayment.core.param.UnifiedOrderParam;
import club.example.wechatpayment.core.result.NotificationResult;
import club.example.wechatpayment.core.result.OrderQueryResult;
import club.example.wechatpayment.core.result.UnifiedOrderResult;
import club.example.wechatpayment.handler.HttpRequestHandler;
import club.example.wechatpayment.provider.PaymentServiceSignProvider;
import club.example.wechatpayment.provider.ServiceParametersProvider;
import club.example.wechatpayment.service.xmlversion.WeChatPayCoreService;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Slf4j
public class WeChatPayCoreServiceImpl implements WeChatPayCoreService {

    private final XmlMapper xmlMapper = new XmlMapper();

    private final ServiceParametersProvider parametersProvider;

    private final PaymentServiceSignProvider signProvider;

    private final HttpRequestHandler httpRequestHandler;

    public WeChatPayCoreServiceImpl(ServiceParametersProvider parametersProvider,
                                    PaymentServiceSignProvider signProvider,
                                    @Qualifier("httpRequestXMLHandler") HttpRequestHandler httpRequestHandler) {
        this.parametersProvider = parametersProvider;
        this.signProvider = signProvider;
        this.httpRequestHandler = httpRequestHandler;
    }


    @Override
    public UnifiedOrderResult unifiedOrder(UnifiedOrderParam param) {
        log.info("unifiedOrderParam = {}", param);
        try {
            String unifiedParamXML = parametersProvider.provide(param);
            log.info("unifiedOrderParam XML = {}",unifiedParamXML);
            String responseBody = httpRequestHandler.handleXmlRequest(unifiedParamXML, UNIFIED_ORDER_URL);
            UnifiedOrderResult orderResult = xmlMapper.readValue(responseBody, UnifiedOrderResult.class);
            Assert.notNull(orderResult, "parse unifiedOrder result failed...");
            return orderResult;
        } catch (IOException e) {
            log.warn("微信统一下单支付请求参数处理异常/ 微信支付序列化异常[请检查参数合法性]: {}", e.getMessage());
        }
        // 微信请求异常，会返回空
        return null;
    }

    @Override
    public OrderQueryResult queryOrder(OrderQueryParam param) {
        log.info("OrderQueryParam = {}", param);
        try {
            String orderQueryParam = parametersProvider.provide(param);
            log.info("orderQuery XML = {}",orderQueryParam);
            String responseBody = httpRequestHandler.handleXmlRequest(orderQueryParam, ORDER_QUERY_URL);
            OrderQueryResult orderResult = xmlMapper.readValue(responseBody, OrderQueryResult.class);
            Assert.notNull(orderResult, "parse unifiedOrder result failed...");
            return orderResult;
        } catch (IOException e) {
            log.warn("微信统一下单支付请求参数处理异常/ 微信支付序列化异常[请检查参数合法性]: {}", e.getMessage());
        }
        // 微信支付序列化异常，会返回空
        return null;
    }

    @Override
    public NotificationResult processNotification(String notificationXMLMessage) {
        log.info("notify xml : {}", notificationXMLMessage);
        if (StringUtils.isEmpty(notificationXMLMessage)) {
            throw new IllegalArgumentException("微信通知回调数据为空...");
        }
        try {
            NotificationResult notificationResult = xmlMapper.readValue(
                    notificationXMLMessage, NotificationResult.class);
            String originalSignKey = notificationResult.getSign();
            String generateSign = signProvider.provide(notificationResult, NotificationResult.class);

            if (StringUtils.isEmpty(originalSignKey) || StringUtils.isEmpty(generateSign)) {
                log.warn("generate sign key was empty or (original sign was empty)");
                return null;
            }
            if (originalSignKey.equals(generateSign)) {
                return notificationResult;
            }
            log.error("sign not matched original = {}, generate sign = {}", originalSignKey, generateSign);
        } catch (IOException e) {
            log.warn("微信序反向列化参数异常 ：{}",e.getMessage());
        }
        // 签名不匹配返回 null
        return null;
    }
}
