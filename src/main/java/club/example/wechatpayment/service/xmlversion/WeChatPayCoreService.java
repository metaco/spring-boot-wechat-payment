package club.example.wechatpayment.service.xmlversion;

import club.example.wechatpayment.core.param.OrderQueryParam;
import club.example.wechatpayment.core.param.UnifiedOrderParam;
import club.example.wechatpayment.core.result.NotificationResult;
import club.example.wechatpayment.core.result.OrderQueryResult;
import club.example.wechatpayment.core.result.UnifiedOrderResult;

public interface WeChatPayCoreService {

    String UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    String ORDER_QUERY_URL = "https://api.mch.weixin.qq.com/pay/orderquery";

    /**
     * 微信支付统一下单接口
     * @param param UnifiedOrderParam
     * @return UnifiedOrderResult
     */
    UnifiedOrderResult unifiedOrder(UnifiedOrderParam param);
    /**
     * 微信订单状态查询接口
     * @param param OrderQueryParam
     * @return OrderQueryResult
     */
    OrderQueryResult queryOrder(OrderQueryParam param);
    /**
     * 微信支付通知处理
     * @param notificationXMLMessage String
     * @return NotificationResult
     */
    NotificationResult processNotification(String notificationXMLMessage);
}
