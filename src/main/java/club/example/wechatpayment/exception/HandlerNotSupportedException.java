package club.example.wechatpayment.exception;

public class HandlerNotSupportedException extends RuntimeException {

    public HandlerNotSupportedException(String message) {
        super(message);
    }
}
