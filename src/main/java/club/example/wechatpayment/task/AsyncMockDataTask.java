package club.example.wechatpayment.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class AsyncMockDataTask {
    /**
     * 没有返回的 异步任务
     */
    @Async
    public void processingMockData() {
        log.info("asyncMockData task ,current thread : {}", Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(3);
            // 脱离主线程 执行任务
            throw new RuntimeException("mock failed");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Async
    public Future<Integer> processingMockDataInteger() {
        log.info("asyncMockDataInteger current thread :{}", Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(3);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new AsyncResult<>(1000);
    }
}
