package club.example.wechatpayment.handler;

public interface HttpRequestHandler {

    String handleXmlRequest(String xmlRequestParam, String requestUrl);

    String handleJsonRequest(String jsonRequestParam, String requestUrl);
}
