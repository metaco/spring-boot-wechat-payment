package club.example.wechatpayment.handler;

import club.example.wechatpayment.exception.HandlerNotSupportedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;

@Slf4j
@Component("httpRequestXMLHandler")
public class HttpRequestXMLHandler implements HttpRequestHandler {

    private final RestTemplate restTemplate = new RestTemplate();

    private final MultiValueMap<String, String> headers = new HttpHeaders();


    @PostConstruct
    public void init() {
        // 这里 要覆盖 原有的 StringHttpMessageConverter
        // 原始索引位置 1
        restTemplate.getMessageConverters()
                .set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));

        headers.add("Content-Type", MediaType.APPLICATION_XML_VALUE + ";charset=UTF8");
    }

    @Override
    public String handleXmlRequest(String xmlRequestParam, String requestUrl) {
        HttpEntity<String> requestEntity = new HttpEntity<>(xmlRequestParam, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(requestUrl, requestEntity, String.class);
        log.info("response = {}", responseEntity.toString());
        log.info("response.headers = {}", responseEntity.getHeaders());

        if (! responseEntity.getStatusCode().equals(HttpStatus.OK)) {
            throw new IllegalStateException(responseEntity.getHeaders().toString());
        }

        if (!responseEntity.hasBody()) {
            throw new IllegalStateException("unifiedOrder;[empty response body]");
        }
        String responseEntityBody = responseEntity.getBody();
        assert responseEntityBody != null;
        String body = new String(responseEntityBody.getBytes(StandardCharsets.UTF_8));
        log.info("response body = {}",body);
        return body;
    }

    @Override
    public String handleJsonRequest(String jsonRequestParam, String requestUrl) {
        throw new HandlerNotSupportedException("不支持json请求内容");
    }
}
