package club.example.wechatpayment.core.constant;

public interface WeChatPayParameters {

    String AND_CHAR = "&";

    String EQUALS_CHAR = "=";

    String SIGN_KEY = "key";
}
