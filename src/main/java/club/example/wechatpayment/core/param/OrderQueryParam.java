package club.example.wechatpayment.core.param;

import club.example.wechatpayment.core.annotation.SignIgnoreProperty;
import club.example.wechatpayment.core.annotation.SignProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JacksonXmlRootElement(localName = "xml")
public class OrderQueryParam {

    @SignProperty("appid")
    @JacksonXmlProperty(localName = "appid")
    private String appId;

    @SignProperty("mch_id")
    @JacksonXmlProperty(localName = "mch_id")
    private String mchId;

    @SignProperty(value = "out_trade_no", required = false)
    @JacksonXmlProperty(localName = "out_trade_no")
    private String outTradeNo;

    @SignProperty(value = "transaction_id",required = false)
    @JacksonXmlProperty(localName = "transaction_id")
    private String transactionId;

    @SignProperty("nonce_str")
    @JacksonXmlProperty(localName = "nonce_str")
    private String nonceStr;

    @SignIgnoreProperty
    private String sign;
}
