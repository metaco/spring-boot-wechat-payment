package club.example.wechatpayment.core.param;

import club.example.wechatpayment.core.annotation.SignIgnoreProperty;
import club.example.wechatpayment.core.annotation.SignProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JacksonXmlRootElement(localName = "xml")
public class UnifiedOrderParam {

    @SignProperty("appid")
    @JacksonXmlProperty(localName = "appid")
    private String appId;

    @SignProperty("mch_id")
    @JacksonXmlProperty(localName = "mch_id")
    private String mchId;

    @SignProperty(value = "device_info", required = false)
    @JacksonXmlProperty(localName = "device_info")
    private String deviceInfo;

    @SignProperty("nonce_str")
    @JacksonXmlProperty(localName = "nonce_str")
    private String nonceStr;

    @SignIgnoreProperty
    private String sign;

    @SignProperty(value = "sign_type", required = false)
    @JacksonXmlProperty(localName = "sign_type")
    private String signType;

    private String body;

    @SignProperty(value = "detail", required = false)
    private String detail;

    private String attach;

    @SignProperty("out_trade_no")
    @JacksonXmlProperty(localName = "out_trade_no")
    private String outTradeNo;

    @SignProperty(value = "fee_type", required = false)
    @JacksonXmlProperty(localName = "fee_type")
    private String feeType;

    @SignProperty("total_fee")
    @JacksonXmlProperty(localName = "total_fee")
    private int totalFee;

    /**
     * 终端IP	spbill_create_ip	是	String(64)	123.12.12.123	支持IPV4和IPV6两种格式的IP地址。调用微信支付API的机器IP
     */
    @SignProperty("spbill_create_ip")
    @JacksonXmlProperty(localName = "spbill_create_ip")
    private String spBillCreateIp;

    //yyyyMMddHHmmss
    @SignProperty(value = "time_start", required = false)
    @JacksonXmlProperty(localName = "time_start")
    private String timeStart;

    @SignProperty(value = "time_expire", required = false)
    @JacksonXmlProperty(localName = "time_expire")
    private String timeExpire;

    @SignProperty("notify_url")
    @JacksonXmlProperty(localName = "notify_url")
    private String notifyUrl;

    @SignProperty("trade_type")
    @JacksonXmlProperty(localName = "trade_type")
    private String tradeType;

    //
    private String receipt;
}
