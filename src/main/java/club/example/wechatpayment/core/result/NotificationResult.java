package club.example.wechatpayment.core.result;

import club.example.wechatpayment.core.annotation.SignIgnoreProperty;
import club.example.wechatpayment.core.annotation.SignProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 * <xml>
 * 	<appid>
 * 		<![CDATA[wx9abed099f0585556]]>
 * 	</appid>
 * 	<attach>
 * 		<![CDATA[{"orderType":1,"companyId":3,"orderId":22561,"payId":2,"couponId":0,"activityType":0}]]>
 * 	</attach>
 * 	<bank_type>
 * 		<![CDATA[OTHERS]]>
 * 	</bank_type>
 * 	<cash_fee>
 * 		<![CDATA[162353]]>
 * 	</cash_fee>
 * 	<fee_type>
 * 		<![CDATA[CNY]]>
 * 	</fee_type>
 * 	<is_subscribe>
 * 		<![CDATA[N]]>
 * 	</is_subscribe>
 * 	<mch_id>
 * 		<![CDATA[1509559041]]>
 * 	</mch_id>
 * 	<nonce_str>
 * 		<![CDATA[49xiarspez4hqzgpdu87c8yjfzu78f6w]]>
 * 	</nonce_str>
 * 	<openid>
 * 		<![CDATA[ogplK1OXhWOhm-Dx5aJel3RBA8AY]]>
 * 	</openid>
 * 	<out_trade_no>
 * 		<![CDATA[b19050516152916637484671hsmb]]>
 * 	</out_trade_no>
 * 	<result_code>
 * 		<![CDATA[SUCCESS]]>
 * 	</result_code>
 * 	<return_code>
 * 		<![CDATA[SUCCESS]]>
 * 	</return_code>
 * 	<sign>
 * 		<![CDATA[CCA2BA732D498366E86D8833B1C2DCA0]]>
 * 	</sign>
 * 	<time_end>
 * 		<![CDATA[20200401164031]]>
 * 	</time_end>
 * 	<total_fee>162353</total_fee>
 * 	<trade_type>
 * 		<![CDATA[APP]]>
 * 	</trade_type>
 * 	<transaction_id>
 * 		<![CDATA[4200000483202004011953533263]]>
 * 	</transaction_id>
 * </xml>
 */
@Setter
@Getter
@JacksonXmlRootElement(localName = "xml")
public class NotificationResult {

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "return_code")
    @SignProperty(value = "return_code")
    private String returnCode;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "appid")
    @SignProperty(value = "appid")
    private String appId;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "mch_id")
    @SignProperty(value = "mch_id")
    private String mchId;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "nonce_str")
    @SignProperty(value = "nonce_str")
    private String nonceStr;

    @JacksonXmlCData
    @SignIgnoreProperty
    private String sign;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "result_code")
    @SignProperty(value = "result_code")
    private String resultCode;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "total_fee")
    @SignProperty(value = "total_fee")
    private int totalFee;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "fee_type")
    @SignProperty(value = "fee_type")
    private String feeType;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "cash_fee")
    @SignProperty(value = "cash_fee")
    private int cashFee;

    @JacksonXmlCData
    @SignProperty(value = "out_trade_no", required = false)
    @JacksonXmlProperty(localName = "out_trade_no")
    private String outTradeNo;

    @JacksonXmlCData
    @SignProperty(value = "bank_type")
    @JacksonXmlProperty(localName = "bank_type")
    private String bankType;

    @JacksonXmlCData
    private String openid;

    @JacksonXmlCData
    @SignProperty(value = "is_subscribe")
    @JacksonXmlProperty(localName = "is_subscribe")
    private String isSubscribe;

    @JacksonXmlCData
    @SignProperty(value = "transaction_id")
    @JacksonXmlProperty(localName = "transaction_id")
    private String transactionId;

    @JacksonXmlCData
    private String attach;

    @JacksonXmlCData
    @SignProperty("trade_type")
    @JacksonXmlProperty(localName = "trade_type")
    private String tradeType;

    @JacksonXmlCData
    @SignProperty("time_end")
    @JacksonXmlProperty(localName = "time_end")
    private String timeEnd;
}
