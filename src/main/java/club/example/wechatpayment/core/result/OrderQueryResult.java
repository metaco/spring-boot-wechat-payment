package club.example.wechatpayment.core.result;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JacksonXmlRootElement(localName = "xml")
public class OrderQueryResult {

    /**
     * <return_msg><![CDATA[OK]]></return_msg>
     * <appid><![CDATA[wx94f03f936f56cd09]]></appid>
     * <mch_id><![CDATA[1491096202]]></mch_id>
     * <device_info><![CDATA[app-v]]></device_info>
     * <nonce_str><![CDATA[DUEnP8hrvHOHuVcE]]></nonce_str>
     * <sign><![CDATA[4079290B3771B425428F3ED114B255DA]]></sign>
     * <result_code><![CDATA[SUCCESS]]></result_code>
     * <total_fee>10</total_fee>
     * <out_trade_no><![CDATA[923845928592489634906806]]></out_trade_no>
     * <trade_state><![CDATA[NOTPAY]]></trade_state>
     * <trade_state_desc><![CDATA[订单未支付]]></trade_state_desc>
     * </xml>
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "return_code")
    private String returnCode;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "return_msg")
    private String returnMsg;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "appid")
    private String appId;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "mch_id")
    private String mchId;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "nonce_str")
    private String nonceStr;

    @JacksonXmlCData
    private String sign;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "total_fee")
    private int totalFee;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "out_trade_no")
    private String outTradeNo;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "trade_state")
    private String tradeState;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "trade_state_desc")
    private String tradeStateDesc;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "device_info")
    private String deviceInfo;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "result_code")
    private String resultCode;
}
