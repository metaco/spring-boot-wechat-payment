package club.example.wechatpayment.core.result;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 * <xml><return_code><![CDATA[SUCCESS]]></return_code>
 * <return_msg><![CDATA[OK]]></return_msg>
 * <appid><![CDATA[wx94f03f936f56cd09]]></appid>
 * <mch_id><![CDATA[1491096202]]></mch_id>
 * <device_info><![CDATA[app-v]]></device_info>
 * <nonce_str><![CDATA[yRd6ZrzF6I89wjbk]]></nonce_str>
 * <sign><![CDATA[1DD3679A06911D3A6BC308264D2A98A5]]></sign>
 * <result_code><![CDATA[SUCCESS]]></result_code>
 * <prepay_id><![CDATA[wx05103029341855ceb2394ba71763483700]]></prepay_id>
 * <trade_type><![CDATA[APP]]></trade_type>
 * </xml>
 */
@Setter
@Getter
@JacksonXmlRootElement(localName = "xml")
public class UnifiedOrderResult {

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "return_code")
    private String returnCode;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "return_msg")
    private String returnMsg;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "appid")
    private String appId;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "mch_id")
    private String mchId;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "nonce_str")
    private String nonceStr;

    @JacksonXmlCData
    private String sign;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "result_code")
    private String resultCode;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "prepay_id")
    private String prepayId;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "trade_type")
    private String tradeType;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "device_info")
    private String deviceInfo;
}
