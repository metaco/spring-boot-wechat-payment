//package club.example.wechatpayment;
//
//import org.springframework.boot.Banner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.WebApplicationType;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.scheduling.annotation.EnableAsync;
//
//@EnableAsync
//@SpringBootApplication
//public class WeChatPaymentApplication {
//
//    public static void main(String[] args) {
//        // 通过使用 SpringApplication.run
//        // SpringApplication.run(WeChatPaymentApplication.class, args);
//
//        // 通过 调整 API 行为
////        SpringApplication application = new SpringApplication(WeChatPaymentApplication.class);
////        application.setBannerMode(Banner.Mode.OFF);
////        application.setWebApplicationType(WebApplicationType.SERVLET);
////        application.run(args);
//
//        // SpringApplicationBuilder Fluent 链式调整
//        new SpringApplicationBuilder(WeChatPaymentApplication.class)
//                .bannerMode(Banner.Mode.OFF)
//                .web(WebApplicationType.SERVLET)
//                .run(args);
//
//        // SpringBoot Application 自动配置原理
//
//    }
//
//}
